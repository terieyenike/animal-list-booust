# Creating a simple HTML file that can read Javascript

**The task is to set up an array with a list of animals by calling by a function.**

#### How was I able to achieve setting up this task, it was by using **git bash** and **initializing the project** with _git init_

To begin the game, the user has to click on the **_Guess Animal_** button so as to input and guess the right animal in the prompt window. 

If you guess right, it logs out onto the screen the right answer, otherwise it tells you to play again.


## Click Here to [view the site](https://my-animal-guess-game.herokuapp.com/)


The programme was developed and written by [Teri Eyenike](http:www.joshuaeyenike.com.ng) **_The Digital Tech Nomad_**
&copy; 2018